import os
import re
import shutil
from pathlib import Path

from combinator import combinator

PROD_DEALER = '3394-PassportMazda'
PROD_EMAIL = 'PassportMazdaMD_SM19v'
CAMPAIGN_CODE = 'PassportMazda_MD_SM19v'
EMAIL_NUM = 'SM19v'

# Do not touch
PROD_ROOT = "//iras.users.autosweet.com/Data/SweetMarketingCampaigns/ProductionEmails/"
IMAGE_ROOT = '//filer3.servers.autosweet.com/files/imageassets/EmailCampaigns/'
UTM_BASE = "utm_source=AutoSweet&utm_medium=email&utm_campaign="
URL_REGEX = re.compile("http\S+")

UTM_CODES = UTM_BASE + CAMPAIGN_CODE


def main():
    email_folder = Path(PROD_ROOT, PROD_DEALER, PROD_EMAIL)
    image_assets = Path(IMAGE_ROOT, PROD_DEALER, EMAIL_NUM)
    os.chdir(email_folder)
    if not os.path.exists(image_assets):
        os.makedirs(image_assets)
    for i in os.listdir('images'):
        if i.endswith('png'):
            try:
                ipath = Path.joinpath(email_folder, 'images', i)
                shutil.copy(ipath, image_assets)
            except:
                continue

    with open('readme.txt', 'r+t') as rf, open('readme_utm.txt', 'w+t') as wuf:
        reader = rf.read()

        matches = URL_REGEX.findall(reader)
        codes = list()
        for i, m in enumerate(matches, start=1):
            codes.append(combinator(m, UTM_CODES, i) + '\n')
        wuf.writelines(codes)


if __name__ == '__main__':
    main()
