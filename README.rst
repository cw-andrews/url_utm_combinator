Install Requirements via pip
============================
`pip install -r requirements.txt`

Run Tests via pytest
====================
`pytest -v .`

Use
====================
.. code:: python
from combinator import combinator
starting_url = 'https://www.bobchapmanford.com/used-vehicles/#action=im_ajax_call&perform=get_results&search=1FAFP44493F380019&page=1&show_all_filters=false&_post_id=5'
utm_codes = 'utm_source=AutoSweet&utm_campaign=DemoDealerSM1'

combinator(starting_url, utm_codes)
'https://www.bobchapmanford.com/used-vehicles/?utm_source=AutoSweet&utm_campaign=DemoDealerSM1#action=im_ajax_call&perform=get_results&search=1FAFP44493F380019&page=1&show_all_filters=false&_post_id=5'

