import os
import shutil
from pathlib import Path
from urllib.parse import parse_qs, urlencode, urlsplit, urlunsplit

import requests
from bs4 import BeautifulSoup


def combinator(url: str, utm_codes: str, utm_content: int) -> str:
    """
    Combine existing URL which may or may not have a query string with
    an additional utm code query string and return new URL as a str.
    """

    if isinstance(url, str) and isinstance(utm_codes, str):
        split_url = urlsplit(url)

        query_dict = parse_qs(split_url.query)
        query_dict.update(parse_qs(utm_codes))
        query_dict = {i[0]: i[1][0] for i in query_dict.items()}
        query_dict['utm_content'] = str(utm_content)
        rebuilt_query_string = urlencode(query_dict)

        return urlunsplit((split_url.scheme,
                           split_url.netloc,
                           split_url.path,
                           rebuilt_query_string,
                           split_url.fragment))

    else:
        raise ValueError("Both URL (str) and utm_codes (str) are required.")


def get_urls(filepath: str) -> list:
    import re

    URL_REGEX = re.compile("http\S+")

    with open(filepath, 'r+t') as rf:
        reader = rf.read()
        matches = URL_REGEX.findall(reader)
        urls = tuple(url for url in matches)
    return urls


def code_urls(urls: tuple, utm_codes: str) -> tuple:
    codes = tuple(
        combinator(m, utm_codes, i) for i, m in enumerate(urls, start=1))
    return codes


def get_title(url: str) -> str:
    html = requests.get(url).text
    title = BeautifulSoup(html, 'html5lib').find('title').text
    return title


def get_q_string_vals(url: str) -> tuple:
    split_url = urlsplit(url)
    query_dict = parse_qs(split_url.query)
    return tuple(v[0] for v in query_dict.values())


def main():
    email_folder = Path(PROD_ROOT, PROD_DEALER, PROD_EMAIL)
    image_assets = Path(IMAGE_ROOT, PROD_DEALER, EMAIL_NUM)
    os.chdir(email_folder)
    if not os.path.exists(image_assets):
        os.makedirs(image_assets)
    for i in os.listdir('images'):
        if i.endswith('png'):
            try:
                ipath = Path.joinpath(email_folder, 'images', i)
                shutil.copy(ipath, image_assets)
            except:
                continue

    urls = get_urls(p.joinpath('readme.txt'))
    titles = tuple(get_title(url) for url in urls)
    qvalues = tuple(get_q_string_vals(url) for url in urls)
    coded_urls = code_urls(urls, utm_codes=UTM_CODES)
    coded_all = zip(coded_urls, titles, qvalues)

    lines = list()
    for i, coded in enumerate(coded_all, start=1):
        u, t, q = coded
        line = '\n'.join([str(i), str(u), str(t), ' '.join(q), '\n'])
        lines.append(line)

    with open('readme_utm.txt', 'w+t') as wf:
        wf.writelines(lines)
