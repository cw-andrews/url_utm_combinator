import pytest

from combinator import combinator

TEST_URL = "https://gitlab.com/cw-andrews?utm_place=special"
TEST_UTM_CODES = "utm_source=AutoSweet&utm_campaign=DemoDealerSM1"
TEST_JOINED = TEST_URL + "?" + TEST_UTM_CODES


class TestCombinator(object):
    def test_required_args(self):
        with pytest.raises(TypeError):
            combinator()
        with pytest.raises(ValueError):
            combinator(url=TEST_URL, utm_codes=None)
        with pytest.raises(ValueError):
            combinator(url=None, utm_codes=TEST_UTM_CODES)
        with pytest.raises(ValueError):
            combinator(url=1, utm_codes=dict())

    def test_return_values(self):
        assert isinstance(combinator(TEST_URL, TEST_UTM_CODES), str)
        assert TEST_URL in combinator(TEST_URL, TEST_UTM_CODES)
        assert TEST_UTM_CODES in combinator(TEST_URL, TEST_UTM_CODES)


if __name__ == '__main__':
    pytest.main()
